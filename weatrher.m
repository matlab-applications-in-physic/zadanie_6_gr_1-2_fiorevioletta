% Program written in MatLab
% Creator: Julia Pluta
% Code credits: gitlab.com/jacek.pawlyta 
% All Contents 2019 Copyright © Julia Pluta All rights reserved
% Make sure you have 'MatLab_Pluta' folder on your desktop with certifikat to IMGW and jsonlab patch 

%Downloading the json package
pom = 'C:\Users\student\Desktop\MatLab_Pluta\jsonlab';
addpath(pom);

%Creating a header in a CSV file
head_0 = {'Czestochowa = 1', 'Katowice = 2', 'Opole = 3', 'Station WTTR = 4', 'Station IMGW = 5'};
head_1 = {'Station','City','Year','Month','Day','Hour','Temperature','Sensed temperature', 'Pressure', 'Wind speed', 'Relative humidity'};
head_2 = {' ',' ',' ',' ',' ','UTC', '°C', '°C', 'hPa','km/h', '%'};

%Checking if such a file already exists
if exist('weatherData.csv') == 2 
    break %If so, it stop
else 
fid = fopen('weatherData.csv','w');
fprintf(fid,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',head_0{1},'";"',head_0{2},'";"',head_0{3},'";"',head_0{4},'";"',head_0{5},'"');
fprintf(fid,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',head_1{1},'";"',head_1{2},'";"',head_1{3},'";"',head_1{4},'";"',head_1{5},'";"',head_1{6},'";"',head_1{7},'";"',head_1{8},'";"',head_1{9},'";"',head_1{10},'";"',head_1{11},'"');
fprintf(fid,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',head_2{1},'";"',head_2{2},'";"',head_2{3},'";"',head_2{4},'";"',head_2{5},'";"',head_2{6},'";"',head_2{7},'";"',head_2{8},'";"',head_2{9},'";"',head_2{10},'";"',head_2{11},'"');
fclose(fid);
end %If it doesn't exist, it creates a CSV file and writes a header to it

%Loop supplementing the CSV file and the matrix
for x = 1:1E12
    str1_x = urlread('http://wttr.in/Czestochowa?format=j1'); %Loading the data path from the weather station
    current1_x = loadjson(str1_x); %Reading them in json format
    cell1_x = struct2cell(current1_x); %Conversion into cells
    mat1_x = cell2mat(cell1_x{1});
    st1_x = 4; %Type of station
    m1_x = 1; %Place
    t1_x = mat1_x.temp_C; %Temperature
    p1_x = mat1_x.pressure; %Pressure
    w1_x = mat1_x.windspeedKmph; %Wind speed
    h1_x = mat1_x.humidity; %Humidity
    time1_x = mat1_x.observation_time; %Time
    e1_x = sscanf(time1_x, '%d');
    if (time1_x(7) == 'A' || (e1_x == 12) || (e1_x == 0))
        e1_x = e1_x; %If it is before noon, the time remains as it was
    else 
        e1_x = e1_x+ 12; %If not, we add 12 to the hour
    end
    data1_x{1,2} = current1_x.weather{1,1}.date;
    da1_x = sscanf(data1_x{1,2}, '%d'); %Date
    to1_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(w1_x))) - (0.0124 * str2double(w1_x))) * ((str2double(t1_x) - 33))); %The formula for the perceived air temperature
    v1_x = [st1_x, m1_x, da1_x(1,1), abs(da1_x(2,1)), abs(da1_x(3,1)), e1_x(1,1), str2double(t1_x), to1_x, str2double(p1_x), str2double(w1_x), str2double(h1_x)]; %Matrix with data related to a given station    
    dlmwrite('weatherData.csv',v1_x, '-append','delimiter', ';'); %Append to the CSV file
    
    str2_x = urlread('http://wttr.in/Katowice?format=j1');
    current2_x = loadjson(str2_x);
    cell2_x = struct2cell(current2_x);
    mat2_x = cell2mat(cell2_x{1});
    st2_x = 4;
    m2_x = 2;
    t2_x = mat2_x.temp_C;
    p2_x = mat2_x.pressure;
    w2_x = mat2_x.windspeedKmph;
    h2_x = mat2_x.humidity;
    time2_x = mat2_x.observation_time;
    e2_x = sscanf(time2_x, '%d');
    if (time2_x(7) == 'A' || (e2_x == 12)|| (e2_x == 0))
        e2_x = e2_x;
    else 
        e2_x = e2_x+ 12;
    end
    data2_x{1,2} = current2_x.weather{1,1}.date;
    da2_x = sscanf(data2_x{1,2}, '%d');
    to2_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(w2_x))) - (0.0124 * str2double(w2_x))) * ((str2double(t2_x) - 33)));
    v2_x = [st2_x, m2_x, da2_x(1,1), abs(da2_x(2,1)), abs(da2_x(3,1)), e2_x(1,1) str2double(t2_x), to2_x, str2double(p2_x), str2double(w2_x), str2double(h2_x)];    
    dlmwrite('weatherData.csv',v2_x, '-append','delimiter', ';');
    
    str3_x = urlread('http://wttr.in/Opole?format=j1');
    current3_x = loadjson(str3_x);
    cell3_x = struct2cell(current3_x);
    mat3_x = cell2mat(cell3_x{1});
    st3_x = 4;
    m3_x = 3;
    t3_x = mat3_x.temp_C;
    p3_x = mat3_x.pressure;
    w3_x = mat3_x.windspeedKmph;
    h3_x = mat3_x.humidity;
    time3_x = mat3_x.observation_time;
    e3_x = sscanf(time3_x, '%d');
    if (time3_x(7) == 'A' || (e3_x == 12)|| (e3_x == 0))
        e3_x = e3_x;
    else 
        e3_x = e3_x+ 12;
    end
    data3_x{1,2} = current3_x.weather{1,1}.date;
    da3_x = sscanf(data3_x{1,2}, '%d');
    to3_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(w3_x))) - (0.0124 * str2double(w3_x))) * ((str2double(t3_x) - 33)));
    v3_x = [st3_x, m3_x, da3_x(1,1), abs(da3_x(2,1)), abs(da3_x(3,1)), e3_x(1,1) str2double(t3_x), to3_x, str2double(p3_x), str2double(w3_x), str2double(h3_x)];    
    dlmwrite('weatherData.csv',v3_x, '-append','delimiter', ';');
    
    str4_x = urlread('https://danepubliczne.imgw.pl/api/data/synop/id/12550/format/json'); %Loading the data path from the weather station
    mat4_x = loadjson(str4_x); %Reading them in json format
    st4_x = 5; %Type of station
    m4_x = 1; %Place
    t4_x = mat4_x.temperatura; %Temperature
    p4_x = mat4_x.cisnienie; %Pressure
    w4_x = mat4_x.predkosc_wiatru; %Wind speed
    h4_x = mat4_x.wilgotnosc_wzgledna;  %Humidity
    e4_x = mat4_x.godzina_pomiaru; %Time
    da4_x = sscanf(mat4_x.data_pomiaru, '%d'); %Date
    to4_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(w4_x))) - (0.0124 * str2double(w4_x))) * ((str2double(t4_x) - 33))); %The formula for the perceived air temperature
    v4_x = [st4_x, m4_x, da4_x(1,1), abs(da4_x(2,1)), abs(da4_x(3,1)), str2double(e4_x), str2double(t4_x), to4_x, str2double(p4_x), str2double(w4_x), str2double(h4_x)]; %Matrix with data related to a given station    
    dlmwrite('weatherData.csv',v4_x, '-append','delimiter', ';'); %Append to the CSV file
    
    str5_x = urlread('https://danepubliczne.imgw.pl/api/data/synop/id/12560/format/json');
    mat5_x = loadjson(str5_x);
    st5_x = 5;
    m5_x = 2;
    t5_x = mat5_x.temperatura;
    p5_x = mat5_x.cisnienie;
    w5_x = mat5_x.predkosc_wiatru;
    h5_x = mat5_x.wilgotnosc_wzgledna;
    e5_x = mat5_x.godzina_pomiaru;
    da5_x = sscanf(mat5_x.data_pomiaru, '%d');
    to5_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(w5_x))) - (0.0124 * str2double(w5_x))) * ((str2double(t5_x) - 33)));
    v5_x = [st5_x, m5_x, da5_x(1,1), abs(da5_x(2,1)), abs(da5_x(3,1)), str2double(e5_x), str2double(t5_x), to5_x, str2double(p5_x), str2double(w5_x), str2double(h5_x)];    
    dlmwrite('weatherData.csv',v5_x, '-append','delimiter', ';');
    
    str6_x = urlread('https://danepubliczne.imgw.pl/api/data/synop/id/12530/format/json');
    mat6_x = loadjson(str6_x);
    st6_x = 5;
    m6_x = 3;
    t6_x = mat6_x.temperatura;
    p6_x = mat6_x.cisnienie;
    w6_x = mat6_x.predkosc_wiatru;
    h6_x = mat6_x.wilgotnosc_wzgledna;
    e6_x = mat6_x.godzina_pomiaru;
    da6_x = sscanf(mat6_x.data_pomiaru, '%d');
    to6_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(w6_x))) - (0.0124 * str2double(w6_x))) * ((str2double(t6_x) - 33)));
    v6_x = [st6_x, m6_x, da6_x(1,1), abs(da6_x(2,1)), abs(da6_x(3,1)), str2double(e6_x), str2double(t6_x), to6_x, str2double(p6_x), str2double(w6_x), str2double(h6_x)];    
    dlmwrite('weatherData.csv',v6_x, '-append','delimiter', ';');
 
%Creating a matrix with header 
A(1,1) = {'Czestochowa = 1'};
A(1,2) = {'Katowice = 2'};
A(1,3) = {'Opole = 3'};
A(1,4) = {'Station WTTR = 4'};
A(1,5) = {'Station IMGW = 5'};
A(2,1) = {'Station'};
A(2,2) = {'City'};
A(2,3) = {'Year'};
A(2,4) = {'Month'};
A(2,5) = {'Day'};
A(2,6) = {'Hour'};
A(2,7) = {'Temperature'};
A(2,8) = {'Sensed temperature'};
A(2,9) = {'Pressure'};
A(2,10) = {'Wind speed'};
A(2,11) = {'Relative humidity'};
A(3,6) = {'UTC'};
A(3,7) = {'°C'};
A(3,8) = {'°C'};
A(3,9) = {'hPa'};
A(3,10) = {'km/h'};
A(3,11) = {'%'};

pom_1 = 'C:\Users\student\Desktop\MatLab_Pluta\weatherData.csv'; 
%Enter the address of the data file into the auxiliary variables
fild_1 = fopen(pom_1);
%Data recovery and a string format accessible to MatLab
C_1 = textscan(fild_1, '%q %q %q %q %q %q %q %q %q %q %q ','Delimiter', ';');
%Checking the size of the CSV file length
pom_2 = dlmread('weatherData.csv', ';',3,0);
pom_3 = size(pom_2);

%The loop creates a matrix with the necessary data
for y = 1: pom_3(1,1)
    B(y,1) = str2num(cell2mat(C_1{1}(y+3)));
    B(y,2) = str2num(cell2mat(C_1{2}(y+3)));
    B(y,3) = str2num(cell2mat(C_1{3}(y+3)));
    B(y,4) = str2num(cell2mat(C_1{4}(y+3)));
    B(y,5) = str2num(cell2mat(C_1{5}(y+3)));
    B(y,6) = str2num(cell2mat(C_1{6}(y+3)));
    B(y,7) = str2num(cell2mat(C_1{7}(y+3)));
    B(y,8) = str2num(cell2mat(C_1{8}(y+3)));
    B(y,9) = str2num(cell2mat(C_1{9}(y+3)));
    B(y,10) = str2num(cell2mat(C_1{10}(y+3)));
    B(y,11) = str2num(cell2mat(C_1{11}(y+3)));
end

%Writing information on the screen about current data from the station
C = [v1_x, v2_x, v3_x, v4_x, v5_x, v6_x];
A
C 

% Finding the maximum and minimum 
L_max_temper = max(B(:,7)); %Finding the maximum extreme
[x_max_temper,y_max_temper] = find(B(:,7) == L_max_temper); 
%Search column and row number corresponding to value a maximum extreme
L_min_temper = min(B(:,7)); %Finding the minimum extreme
%Search column and row number corresponding to value a minimum extreme
[x_min_temper,y_min_temper] = find(B(:,7) == L_min_temper);

% Finding the maximum and minimum 
L_max_odcz = max(B(:,8));
[x_max_odcz,y_max_odcz] = find(B(:,8) == L_max_odcz);
L_min_odcz = min(B(:,8));
[x_min_odcz,y_min_odcz] = find(B(:,8) == L_min_odcz);

% Finding the maximum and minimum 
L_max_cisn = max(B(:,9));
[x_max_cisn,y_max_cisn] = find(B(:,9) == L_max_cisn);
L_min_cisn = min(B(:,9));
[x_min_cisn,y_min_cisn] = find(B(:,9) == L_min_cisn);

% Finding the maximum and minimum 
L_max_wilw = max(B(:,11));
[x_max_wilw,y_max_wilw] = find(B(:,11) == L_max_wilw);
L_min_wilw = min(B(:,11));
[x_min_wilw,y_min_wilw] = find(B(:,11) == L_min_wilw);

%Writing information on the screen
fprintf('\nOn ');
fprintf(num2str(B(x_max_temper(max(size(x_max_temper)),1),3)));
fprintf('-');
fprintf(num2str(B(x_max_temper(max(size(x_max_temper)),1),4)));
fprintf('-');
fprintf(num2str(B(x_max_temper(max(size(x_max_temper)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_max_temper(max(size(x_max_temper)),1),6)));
fprintf(' the maxiumum temperature was: %.2f °C', L_max_temper);
fprintf('\nOn ');
fprintf(num2str(B(x_min_temper(min(size(x_min_temper)),1),3)));
fprintf('-');
fprintf(num2str(B(x_min_temper(min(size(x_min_temper)),1),4)));
fprintf('-');
fprintf(num2str(B(x_min_temper(min(size(x_min_temper)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_min_temper(min(size(x_min_temper)),1),6)));
fprintf(' the minimum temperature was: %.2f °C', L_min_temper);

%Writing information on the screen
fprintf('\nOn ');
fprintf(num2str(B(x_max_odcz(max(size(x_max_odcz)),1),3)));
fprintf('-');
fprintf(num2str(B(x_max_odcz(max(size(x_max_odcz)),1),4)));
fprintf('-');
fprintf(num2str(B(x_max_odcz(max(size(x_max_odcz)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_max_odcz(max(size(x_max_odcz)),1),6)));
fprintf(' the maxiumum sensed temperature was: %.2f °C', L_max_odcz);
fprintf('\nOn ');
fprintf(num2str(B(x_min_odcz(min(size(x_min_odcz)),1),3)));
fprintf('-');
fprintf(num2str(B(x_min_odcz(min(size(x_min_odcz)),1),4)));
fprintf('-');
fprintf(num2str(B(x_min_odcz(min(size(x_min_odcz)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_min_odcz(min(size(x_min_odcz)),1),6)));
fprintf(' the minimum sensed temperature was: %.2f °C', L_min_odcz);

%Writing information on the screen
fprintf('\nOn ');
fprintf(num2str(B(x_max_cisn(max(size(x_max_cisn)),1),3)));
fprintf('-');
fprintf(num2str(B(x_max_cisn(max(size(x_max_cisn)),1),4)));
fprintf('-');
fprintf(num2str(B(x_max_cisn(max(size(x_max_cisn)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_max_cisn(max(size(x_max_cisn)),1),6)));
fprintf(' the maxiumum pressure was: %.2f °C', L_max_odcz);
fprintf('\nOn ');
fprintf(num2str(B(x_min_cisn(min(size(x_min_cisn)),1),3)));
fprintf('-');
fprintf(num2str(B(x_min_cisn(min(size(x_min_cisn)),1),4)));
fprintf('-');
fprintf(num2str(B(x_min_cisn(min(size(x_min_cisn)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_min_cisn(min(size(x_min_cisn)),1),6)));
fprintf(' the minimum pressure was: %.2f °C', L_min_odcz);

%Writing information on the screen
fprintf('\nOn ');
fprintf(num2str(B(x_max_wilw(max(size(x_max_wilw)),1),3)));
fprintf('-');
fprintf(num2str(B(x_max_wilw(max(size(x_max_wilw)),1),4)));
fprintf('-');
fprintf(num2str(B(x_max_wilw(max(size(x_max_wilw)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_max_wilw(max(size(x_max_wilw)),1),6)));
fprintf(' the maxiumum relative humidity was: %.2f %%', L_max_odcz);
fprintf('\nOn ');
fprintf(num2str(B(x_min_wilw(min(size(x_min_wilw)),1),3)));
fprintf('-');
fprintf(num2str(B(x_min_wilw(min(size(x_min_wilw)),1),4)));
fprintf('-');
fprintf(num2str(B(x_min_wilw(min(size(x_min_wilw)),1),5)));
fprintf(', at ');
fprintf(num2str(B(x_min_wilw(min(size(x_min_wilw)),1),6)));
fprintf(' the minimum relative humidity was: %.2f %%', L_min_odcz);

pause(3600); %The condition makes the loop run every hour
end %The end of this long loop